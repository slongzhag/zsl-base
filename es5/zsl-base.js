/**
* 常用函数封装
* author: slongzhang
* date: 2021-01-06
*/
( function( global, factory ) {
  "use strict";
  if ( typeof module === "object" && typeof module.exports === "object" ) {

    // For CommonJS and CommonJS-like environments where a proper `window`
    // is present, execute the factory and get zslBase.
    // For environments that do not have a `window` with a `document`
    // (such as Node.js), expose a factory as module.exports.
    // This accentuates the need for the creation of a real `window`.
    // e.g. var zslBase = require("zslBase")(window);
    // See ticket #14549 for more info.
    module.exports = global.document ?
      factory( global, true ) :
      function( w ) {
        if ( !w.document ) {
          throw new Error( "zslBase requires a window with a document" );
        }
        return factory( w );
      };
  } else {
    factory( global );
  }

// Pass this if window is not defined yet
} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {
  "use strict";

  function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

  function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

  function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

  function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

  function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e2) { throw _e2; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e3) { didErr = true; err = _e3; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

  // 正则需要转义的字符

  var regEscapeCharacter = ['$', '(', ')', '*', '+', '.', '[', ']', '?', '\\', '/', '^', '{', '}', '!', '|']; // 判断是否为空

  var empty = function empty(value) {
    if (null === value) {
      return true;
    } else if (['Set', 'Map'].includes(Object.prototype.toString.call(value).slice(8, -1))) {
      return value.size == 0 ? true : false;
    } else if ("object" == _typeof(value)) {
      if (Object.getOwnPropertyDescriptor(value, "length")) {
        return value.length > 0 ? false : true;
      } else {
        return Object.keys(value).length > 0 ? false : true;
      }
    } else if (value == 0) {
      return true;
    } else {
      return !value;
    }
  }; // 合成模板内部函数


  var compileHtml = function compileHtml(template) {
    var evalExpr = /<%=(.+?)%>/g;
    var expr = /<%([\s\S]+?)%>/g;
    template = template.replace(evalExpr, '`); \n echo( $1 ); \n echo(`').replace(expr, '`); \n $1 \n echo(`');
    template = 'echo(`' + template + '`);';
    var script = "(function parse(data){\n            let output = \"\";\n            function echo(html){\n                output += html;\n            }\n            ".concat(template, "\n            return output;\n        })");
    return script;
  };

  var zslBase = {};
  zslBase.version = "1.0.1";
  zslBase.author = 'slongzhang@qq.com'; // 从缓存判断是否开启调试

  var debugInfo = localStorage.getItem('zsl-debug');

  if (debugInfo > new Date().valueOf()) {
    zslBase.log = console.log;
  } else {
    // 移除过期debug开关
    localStorage.removeItem('zsl-debug');

    zslBase.log = function () {};
  } // 设置debug开关


  zslBase.debug = function (bool) {
    if (bool) {
      // debug有效期3小时
      localStorage.setItem('zsl-debug', new Date().valueOf() + 10800000);
    } else {
      localStorage.removeItem('zsl-debug');
    }
  };
  /**
  * 加载后才执行
  * _fn            function             一个可执行函数
  */


  zslBase.ready = function (_fn) {
    if (typeof _fn !== 'function') {
      throw 'Fn is not a function!';
    }

    function completed() {
      document.removeEventListener("DOMContentLoaded", completed);
      window.removeEventListener("load", completed);

      _fn();
    }

    if (document.readyState === "complete" || document.readyState !== "loading" && !document.documentElement.doScroll) {
      // Handle it asynchronously to allow scripts the opportunity to delay ready
      window.setTimeout(_fn);
    } else {
      // Use the handy event callback
      document.addEventListener("DOMContentLoaded", completed); // A fallback to window.onload, that will always work

      window.addEventListener("load", completed);
    }
  };
  /**
  * 判断是否json字符串
  * _str          String
  * return        boolean
  .*/


  zslBase.isJson = function (_str) {
    if (typeof _str === 'string') {
      try {
        JSON.parse(_str);
        return true;
      } catch (e) {}
    }

    return false;
  };
  /**
  * 判断是否是数字
  * val           All         任意要检查的变量
  * return        Boolean     返回ture Or false
  .*/


  zslBase.isNumeric = function (_val) {
    // isNaN()函数 把空串 空格 以及NUll 按照0来处理 所以先去除 
    // if(_val === "" || _val === null){
    //     return false;
    // }
    // if(!isNaN(_val)){
    //     //对于空数组和只有一个数值成员的数组或全是数字组成的字符串，isNaN返回false，例如：'123'、[]、[2]、['123'],isNaN返回false,
    //     //所以如果不需要_val包含这些特殊情况，则这个判断改写为if(!isNaN(_val) && typeof _val === 'number' )
    //     return true; 
    // }else{
    //     return false;
    // }
    return _val !== "" && _val !== null && !isNaN(_val);
  };
  /**
  * trim基础函数
  * _string           String                要处理的原始字符串
  * _charlist         String                要去除的字符
  * _type             Integer               去除类型:0两边,1:左边,2右边
  .*/


  var _trimBase = function _trimBase(_string, _charlist) {
    var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

    if (typeof _string !== 'string') {
      return _string;
    }

    if (typeof _charlist === 'undefined') {
      if (type === 1) {
        return _string.replace(/^\s+/gm, '');
      } else if (type === 2) {
        return _string.replace(/\s+$/gm, '');
      } else {
        return _string.replace(/^\s+|\s+$/gm, '');
      }
    }

    _charlist = _charlist.split('');
    var zy = regEscapeCharacter,
        ps = '';

    var _iterator = _createForOfIteratorHelper(_charlist),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        var item = _step.value;

        if (zy.includes(item)) {
          ps += '\\';
        }

        ps += item;
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }

    var reg;

    if (type === 1) {
      reg = new RegExp("\^" + ps + "+", "gm");
    } else if (type === 2) {
      reg = new RegExp(ps + "+$", "gm");
    } else {
      reg = new RegExp("\^" + ps + "+|" + ps + "+$", "gm");
    }

    return _string.replace(reg, '');
  }; // 移除两侧指定字符


  zslBase.trim = function (_string, _charlist) {
    return _trimBase(_string, _charlist, 0);
  }; // 移除左侧指定字符


  zslBase.ltrim = function (_string, _charlist) {
    return _trimBase(_string, _charlist, 1);
  }; // 移除右侧指定字符


  zslBase.rtrim = function (_string, _charlist) {
    return _trimBase(_string, _charlist, 2);
  }; // 判断变量类型


  zslBase.checkType = function (any) {
    return Object.prototype.toString.call(any).slice(8, -1);
  }; // 变量深拷贝


  zslBase.varClone = function (any) {
    var result;

    switch (zslBase.checkType(any)) {
      case 'Object':
        // 拷贝对象
        result = {};

        for (var key in any) {
          result[key] = zslBase.varClone(any[key]);
        }

        break;

      case 'Array':
        // 拷贝数组
        result = [];

        var _iterator2 = _createForOfIteratorHelper(any),
            _step2;

        try {
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            var item = _step2.value;
            result.push(zslBase.varClone(item));
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }

        break;

      case 'Function':
        result = new Function('return ' + any.toString()).call(window);
        break;

      case 'Date':
        result = new Date(any.valueOf());
        break;

      case 'RegExp':
        result = new RegExp(any);
        break;

      case 'Map':
        result = new Map();
        any.forEach(function (v, k) {
          result.set(k, zslBase.varClone(v));
        });
        break;

      case 'Set':
        result = new Set();

        var _iterator3 = _createForOfIteratorHelper(any.values()),
            _step3;

        try {
          for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
            var val = _step3.value;
            result.add(zslBase.varClone(val));
          }
        } catch (err) {
          _iterator3.e(err);
        } finally {
          _iterator3.f();
        }

        break;

      default:
        result = any;
    }

    return result;
  }; // 深度合并对象(有兼容数组，但不建议混用)


  zslBase.deepMerge = function () {
    for (var _len = arguments.length, param = new Array(_len), _key = 0; _key < _len; _key++) {
      param[_key] = arguments[_key];
    }

    // let result = Object.assign({}, ...param);
    // for (let item of param) {
    //     for (let [idx, val] of Object.entries(item)) {
    //         if (typeof val === 'object') {
    //             result[idx] = zslBase.deepMerge(result[idx], val);
    //         }
    //     }   
    // }
    // return result;
    var result,
        isArr = param.every(function (v) {
      return zslBase.checkType(v) === 'Array';
    });

    if (isArr) {
      var _ref;

      // 如果全部是数组则采用数组合并的方法
      result = Array.from(new Set((_ref = []).concat.apply(_ref, param)));
    } else {
      result = Object.assign.apply(Object, [{}].concat(param));

      var _iterator4 = _createForOfIteratorHelper(param),
          _step4;

      try {
        for (_iterator4.s(); !(_step4 = _iterator4.n()).done;) {
          var item = _step4.value;

          for (var _i = 0, _Object$entries = Object.entries(item); _i < _Object$entries.length; _i++) {
            var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
                idx = _Object$entries$_i[0],
                val = _Object$entries$_i[1];

            if (_typeof(val) === 'object') {
              result[idx] = zslBase.deepMerge(result[idx], val);
            }
          }
        }
      } catch (err) {
        _iterator4.e(err);
      } finally {
        _iterator4.f();
      }
    }

    return result;
  };
  /**
  * 获取时间戳，默认毫秒
  * _type                boolean            默认false输出到毫秒，true则输出到秒
  .*/


  zslBase.time = function (_type) {
    var timestemp = new Date().valueOf();

    if (_type) {
      // 返回秒
      timestemp = parseInt(timestemp / 1000);
    }

    return timestemp;
  };
  /**
  * 时间计算器
  * string                String|Integer            日期格式字符串或毫秒时间戳
  * time                  String|Integer|Object     时间字符串|时间戳|new Date()出来的时间对象
  * toms                  Boolean                   转换为秒的时间戳
  * 模拟php的strtotime，但未适配日期时间转时间戳，如需处理如 '2021-01-06'转时间戳请使用'0 s'方法如('0 s','2021-01-06')或直接使用new Date('2021-01-06').valueOf();
  .*/


  zslBase.strtotime = function (string, time) {
    var toSecond = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
    var date, timestamp; // 判断string, time参数是否存在

    if (typeof string === 'undefined' && typeof time === 'undefined') {
      // 全部为空，返回当前时间戳
      return new Date().valueOf();
    } // 如果不为空，有参数传入，判断第一个参数string 是不是时间对象或符合时间正则


    var datetimeRegExp = /^([1-2]\d{3})-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2][0-9]|30|31) (0?\d|1\d|2[0-4]):(0?\d|[1-5]\d):(0?\d|[1-5]\d)$/; // 日期 + 时间

    var dateRegExp = /^([1-2]\d{3})-(0?[1-9]|1[0-2])-(0?[1-9]|[1-2][0-9]|30|31)$/; // 纯日期

    if (zslBase.checkType(string) === 'Date' || datetimeRegExp.test(string) || dateRegExp.test(string)) {
      // 如果第一个参数是日期格式则，time参数变成了toSecond;
      if (dateRegExp.test(string)) {
        string += ' 00:00:00'; // 防止日期小于10导致的读取0点与标准读取格林8点的问题
      }

      timestamp = new Date(string).valueOf();
      return time ? parseInt(timestamp / 1000) : timestamp;
    } // 进行时间计算


    if (!time || time === 'now') {
      date = new Date();
      timestamp = date.valueOf();
    } else {
      date = new Date(time);
      timestamp = date.valueOf();
    }

    var values, unitNames;

    try {
      // 提取数值和单位，只保留数字和加减点
      values = string.replace(/[^\d-+.]+/g, '_#_').split('_#_').filter(function (s) {
        return s && !isNaN(parseInt(s)); // 防止0被过滤
      }); // 单位名,只保留纯字母

      unitNames = string.replace(/[^\a-zA-Z]+/g, '_#_').split('_#_').filter(function (s) {
        return s && s.trim();
      }); // 正则无法匹配到计算规则，则不符合我们参数要求，抛出异常给用户

      if (values.length === 0 || unitNames.length === 0) {
        throw '';
      }
    } catch (error) {
      throw "参数格式错误，请参照 a b a b ……,a：正负数整型，b:单位year(y年),month(m月),week(w周),day(d日),hours(h时),minutes(i分),seconds(s秒)\r\n如: '2 day -1 hours'=》47小时后\r\n如: '-2 day 1 hours'=》47小时前";
    }

    var uniqueUnit = {},
        temp;

    for (var ii = 0; ii < values.length; ii++) {
      if (unitNames[ii] === 'minutes') {
        unitNames[ii] = 'i';
      }

      if (unitNames[ii] && !uniqueUnit.hasOwnProperty(unitNames[ii][0])) {
        var val = parseInt(values[ii]);
        uniqueUnit[unitNames[ii][0]] = 1; // 取单位的首字母

        switch (unitNames[ii]) {
          case 'y': // no break;

          case 'year':
            temp = new Date(timestamp);
            temp.setFullYear(temp.getFullYear() + val);
            timestamp = temp.valueOf();
            break;

          case 'm': // no break;

          case 'month':
            temp = new Date(timestamp);
            temp.setMonth(temp.getMonth() + val);
            timestamp = temp.valueOf();
            break;

          case 'w': // no break;

          case 'week':
            timestamp += val * 604800000;
            break;

          case 'd': // no break;

          case 'day':
            timestamp += val * 86400000;
            break;

          case 'h': // no break;

          case 'hours':
            timestamp += val * 3600000;
            break;

          case 'i': // no break;

          case 'minutes':
            timestamp += val * 60000;
            break;

          case 's': // no break;

          case 'seconds':
            timestamp += val * 1000;
            break;
        }
      }
    }

    return toSecond ? parseInt(timestamp / 1000) : timestamp;
  };
  /**
  * 获取指定日期格式(代完善)
  * _format            String|Integer            日期格式字符串"Y-m-d H:i:s"或毫秒时间戳
  * _time              String|int|object         时间字符串|时间戳|new Date()出来的时间对象
  .*/


  zslBase.date = function () {
    var format = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Y-m-d H:i:s';
    var time = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : new Date();

    if (zslBase.checkType(format) === 'Date') {
      time = zslBase.varClone(format);
      format = 'Y-m-d H:i:s';
    } else if (zslBase.checkType(format) === 'Number') {
      time = new Date(format);
      format = 'Y-m-d H:i:s';
    } else {
      if (zslBase.checkType(format) !== 'String') {
        throw '参数错误';
      }

      time = new Date(time);

      if (time.toString() === 'Invalid Date') {
        time = new Date(null);
      }
    } // 得到"Wed Apr 07 2021" => ['Wed','Apr','07','2021']


    var dateString = time.toDateString().split(' '); // 补位函数

    var strPad = function strPad(v) {
      var len = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
      var vLen = v.toString().length;

      if (vLen < len) {
        for (var i = len - vLen; i > 0; i--) {
          v = '0' + v;
        }
      }

      return v;
    };

    var result = [],
        temp;
    var len = format.length;

    for (var ii = 0; ii < len; ii++) {
      if (format[ii] === '\\') {
        result.push(format[++ii]);
        continue;
      }

      switch (format[ii]) {
        case 'd':
          // 一个月中的第几天（从 01 到 31）
          result.push(strPad(time.getDate()));
          break;

        case 'D':
          // 星期几的文本表示（用三个字母表示）
          result.push(dateString[0]);
          break;

        case 'j':
          // 一个月中的第几天，不带前导零（1 到 31）
          result.push(time.getDate());
          break;
        // case 'l':
        //     // （'L' 的小写形式）- 星期几的完整的文本表示
        //     break;

        case 'N':
          // 星期几的 ISO-8601 数字格式表示（1表示Monday[星期一]，7表示Sunday[星期日]）
          result.push(time.getDay() + 1);
          break;
        // case 'S':
        //     // 一个月中的第几天的英语序数后缀（2 个字符：st、nd、rd 或 th。与 j 搭配使用）
        //     break;

        case 'w':
          // 星期几的数字表示（0 表示 Sunday[星期日]，6 表示 Saturday[星期六]）
          result.push(time.getDay());
          break;

        case 'z':
          // 一年中的第几天（从 0 到 365）
          result.push(Math.ceil((new Date(time.toDateString()) - new Date(time.getFullYear().toString())) / (24 * 60 * 60 * 1000)));
          break;

        case 'W':
          // 用 ISO-8601 数字格式表示一年中的第几个星期（第几周）
          result.push(Math.ceil(Math.ceil((new Date(time.toDateString()) - new Date(time.getFullYear().toString())) / (24 * 60 * 60 * 1000)) / 7));
          break;
        // case 'F':
        //     // 月份的完整的文本表示（January[一月份] 到 December[十二月份]）
        //     break;

        case 'm':
          // 月份的数字表示（从 01 到 12）
          result.push(strPad(time.getMonth() + 1));
          break;

        case 'M':
          // 月份的短文本表示（用三个字母表示）
          result.push(dateString[1]);
          break;

        case 'n':
          // 月份的数字表示，不带前导零（1 到 12）
          result.push(strPad(time.getMonth() + 1));
          break;

        case 't':
          // 给定月份中包含的天数
          temp = new Date(time); // 防止改变原时间对象

          temp.setMonth(time.getMonth() + 1);
          temp.setDate(0); // 设置为0,date会自动切为上一个月的最后一天

          result.push(temp.getDate());
          break;

        case 'L':
          // 是否是闰年（如果是闰年则为 1，否则为 0）
          temp = time.getFullYear();

          if (temp % 4 == 0 && temp % 100 != 0 || temp % 400 == 0) {
            result.push(1);
          } else {
            result.push(0);
          }

          break;

        case 'o': // ISO-8601 标准下的年份数字

        case 'Y':
          // 年份的四位数表示
          result.push(time.getFullYear());
          break;

        case 'y':
          // 年份的两位数表示
          result.push(time.getFullYear().toString().substr(2, 2));
          break;

        case 'a':
          //小写形式表示：am 或 pm
          temp = time.getHours();
          result.push(temp < 12 ? 'am' : 'pm');
          break;

        case 'A':
          // 大写形式表示：AM 或 PM
          temp = time.getHours();
          result.push(temp < 12 ? 'AM' : 'PM');
          break;
        // case 'B':
        //     // Swatch Internet Time（000 到 999）
        //     break;

        case 'g':
          // 12 小时制，不带前导零（1 到 12）
          temp = time.getHours();
          result.push(temp > 12 ? temp - 12 : temp);
          break;

        case 'G':
          // 24 小时制，不带前导零（0 到 23）
          temp = time.getHours();
          result.push(temp);
          break;

        case 'h':
          // 12 小时制，带前导零（01 到 12）
          temp = time.getHours();
          temp = temp > 12 ? temp - 12 : temp;
          result.push(strPad(temp));
          break;

        case 'H':
          // 24 小时制，带前导零（00 到 23）
          temp = time.getHours();
          result.push(strPad(temp));
          break;

        case 'i':
          // 分，带前导零（00 到 59）
          result.push(strPad(time.getMinutes()));
          break;

        case 's':
          // 秒，带前导零（00 到 59）
          result.push(strPad(time.getSeconds()));
          break;

        default:
          result.push(format[ii]);
      }
    }

    return result.join('');
  };
  /**
  * 二维数组排序
  * data             Array
  * field            String
  * type             Boolean|String
  .*/


  zslBase.arraySort = function (data, field, type) {
    var toFloat = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

    if (Array.isArray(data) && data[0].hasOwnProperty(field)) {
      data = JSON.parse(JSON.stringify(data)); // 去除指针关系,防止影响源变量

      var judge = function judge(x, y) {
        if (toFloat && !isNaN(parseInt(x)) && !isNaN(parseInt(y))) {
          x = /^\s*([\d+\.]*)%/.test(x) ? parseFloat(x) / 100 : parseFloat(x), y = /^\s*([\d+\.]*)%/.test(y) ? parseFloat(y) / 100 : parseFloat(y);
        }

        if (x < y) {
          return 1;
        } else if (x > y) {
          return -1;
        } else if (x == y) {
          return 0;
        }
      };

      if (typeof type === 'string') {
        type = type.toLowerCase(); // 全部转换小写

        type = type === 'desc' ? true : type === 'asc' ? false : type;
      }

      if (type) {
        // 倒序
        data.sort(function (x, y) {
          return judge(x[field], y[field]);
        });
      } else {
        // 正序
        data.sort(function (x, y) {
          if (x[field] == '' || y[field] == '') {
            return judge(x[field], y[field]);
          } else {
            return -judge(x[field], y[field]);
          }
        });
      }
    }

    return data;
  };
  /**
  * 注入远程js(还可用于JSONP请求)
  * remoteSrc            String          访问得到得url地址
  * callback             Function        注入完成后执行得函数
  .*/


  zslBase.injectJs = function (remoteSrc, callback) {
    if (remoteSrc) {
      var script = document.createElement('script');
      script.setAttribute('type', 'text/javascript');
      script.src = remoteSrc;

      script.onload = function () {
        // 放在页面不好看，执行完后移除掉
        this.parentNode.removeChild(this);

        if (typeof callback === 'function') {
          callback();
        }
      };

      document.body.appendChild(script);
    }
  };
  /**
  * 注入文本js
  * text                String              script脚本
  * callback            Function            脚本注入完后执行得脚本
  .*/


  zslBase.injectTextJs = function (text, callback) {
    var safeMode = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

    if (text) {
      var script = document.createElement('script');
      script.setAttribute('type', 'text/javascript');

      if (safeMode) {
        // 默认安全模式添加try防止报错
        text = 'try{' + text + '}catch(err){console.log(err)}'; // 加try防止注入不合法js导致后续代码无法执行
      }

      var textNode = document.createTextNode(text); //声明变量并创建文本内容；

      script.appendChild(textNode);
      document.body.appendChild(script);
      setTimeout(function () {
        // 放在页面不好看，执行完后移除掉
        script.parentNode.removeChild(script);

        if (typeof callback === 'function') {
          callback();
        }
      }, 200);
    }
  };
  /**
  * 注入远程css
  * remoteSrc            Url            可访问css文件的合法url地址
  .*/


  zslBase.injectCss = function (remoteSrc) {
    if (remoteSrc) {
      var link = document.createElement('link');
      document.head.appendChild(link);
      link.setAttribute('rel', 'stylesheet');
      link.setAttribute('href', remoteSrc);
      link.setAttribute('type', 'text/css');
    }
  };
  /**
  * 注入本地文本css
  * text              本地css文本样式
  * [attr]            Object            设置Style标签的属性名,可选
  .*/


  zslBase.injectTextCss = function (text, attr) {
    if (text) {
      var style = document.createElement('style');
      style.setAttribute('author', 'slongzhang');
      style.setAttribute('time', Date.now()); // 设置注入时间

      if (!empty(attr) && attr.constructor === Object) {
        // 注入标识,便于识别注入的还是原生的
        attr = Object.entries(attr);

        var _iterator5 = _createForOfIteratorHelper(attr),
            _step5;

        try {
          for (_iterator5.s(); !(_step5 = _iterator5.n()).done;) {
            var item = _step5.value;
            var k = item[0],
                v = item[1];

            if (!empty(k) && !empty(v)) {
              style.setAttribute(k, v);
            }
          }
        } catch (err) {
          _iterator5.e(err);
        } finally {
          _iterator5.f();
        }
      }

      var textNode = document.createTextNode(text); //声明变量并创建文本内容；

      style.appendChild(textNode);
      document.body.appendChild(style);
    }
  };
  /**
  * 获取根域名
  * 一般应用于浏览器插件开发或第三方脚本的(如站长统计等识别根域名等)，自家项目一般用不到
  .*/


  zslBase.getMainHost = function () {
    var key = 'slongzhang_' + Math.random();
    var keyR = new RegExp('(^|;)\\s*' + key + '=12345');
    var expiredTime = new Date(0);
    var domain = document.domain;
    var domainList = domain.split('.');
    var urlItems = []; // 主域名一定会有两部分组成

    urlItems.unshift(domainList.pop()); // 慢慢从后往前测试

    while (domainList.length) {
      urlItems.unshift(domainList.pop());
      var mainHost = urlItems.join('.');
      var cookie = key + '=12345;domain=.' + mainHost;
      document.cookie = cookie; //如果cookie存在，则说明域名合法

      if (keyR.test(document.cookie)) {
        document.cookie = cookie + ';expires=' + expiredTime;
        return mainHost;
      }
    }
  };
  /**
  * 模板转换写成全局函数
  * 示例：
  *   let template = `
  *    <ul>
  *    <% for(let i=0; i < data.supplies.length; i++) { %>
  *       <li><%= data.supplies[i] %></li>
  *     <% } %>
  *    </ul>
  *    `;
  *    console.log(GTemplateOutput(template,{ supplies: [ "broom", "mop", "cleaner" ] }));
  */


  zslBase.outputHtml = function (template, data) {
    if (template) {
      var parse = eval(compileHtml(template));
      return parse(data);
    } else {
      return '';
    }
  };
  /**
  * 将xml转换成document树
  * xml              String            xml的文本字符串
  * return            XML对象(document)
  .*/


  zslBase.parseXML = function (xml) {
    var xmlDoc;

    try {
      if (window.DOMParser) {
        xmlDoc = new DOMParser();
        xmlDoc = xmlDoc.parseFromString(xml, "text/xml");
      } else // Internet Explorer
        {
          xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
          xmlDoc.async = "false";
          xmlDoc.loadXML(xml);
        }
    } catch (e) {
      xmlDoc = xml;
    }

    return xmlDoc;
  };
  /**
  * 全局Eval函数(同window.eval())
  .*/


  zslBase.globalEval = function (data) {
    // 如果script字符串存在，去除左右空格
    if (data) {
      // We use execScript on Internet Explorer
      // We use an anonymous function so that context is window
      return (window.execScript || function (data) {
        return window["eval"].call(window, data);
      })(data);
    }
  };
  /**
  * httpBuildQuery
  * object            Object            要转换成http_query的对象
  .*/


  zslBase.httpBuildQuery = function (object) {
    var query = [];

    if (object && object.constructor === Object) {
      object = Object.entries(object);

      var _iterator6 = _createForOfIteratorHelper(object),
          _step6;

      try {
        for (_iterator6.s(); !(_step6 = _iterator6.n()).done;) {
          var item = _step6.value;

          if (item[1] && _typeof(item[1]) == 'object') {
            item[1] = JSON.stringify(item[1]);
          }

          var k = item[0],
              v = encodeURIComponent(item[1]);
          query.push(k + '=' + v);
        }
      } catch (err) {
        _iterator6.e(err);
      } finally {
        _iterator6.f();
      }
    }

    return query.join('&');
  };
  /**
  * 恢复http_query
  * 将http_query转换成对象{}
  .*/


  zslBase.httpQueryObject = function (string) {
    if (!string) {
      return {};
    }

    var result = {},
        arr = string.split('&');

    var _iterator7 = _createForOfIteratorHelper(arr),
        _step7;

    try {
      for (_iterator7.s(); !(_step7 = _iterator7.n()).done;) {
        var item = _step7.value;
        var k = void 0,
            v = void 0,
            len = item.indexOf('=');
        len && (k = item.substr(0, len), v = decodeURIComponent(item.substr(len + 1)), result[k] = zslBase.isJson(v) ? JSON.parse(v) : v);
      }
    } catch (err) {
      _iterator7.e(err);
    } finally {
      _iterator7.f();
    }

    return result;
  }; // cookie解析


  zslBase.cookieQueryObject = function (cookie) {
    if (cookie && typeof cookie == 'string') {
      cookie = cookie.split('; ');
    }

    var result = {};

    var _iterator8 = _createForOfIteratorHelper(cookie),
        _step8;

    try {
      for (_iterator8.s(); !(_step8 = _iterator8.n()).done;) {
        var item = _step8.value;
        var len = item.indexOf('=');
        result[item.substr(0, len)] = item.substr(len + 1);
      }
    } catch (err) {
      _iterator8.e(err);
    } finally {
      _iterator8.f();
    }

    return result;
  }; // ajax


  zslBase.ajax = function (option) {
    // 不能为空,不能不是配置参数
    if (!option || option.constructor !== Object || !Object.keys(option).length) {
      throw 'Fatal error, please pass in the correct parameter';
    }

    var xhr;

    if (window.XMLHttpRequest) {
      xhr = new XMLHttpRequest();
    } else {
      xhr = new ActiveXObject('Microsoft.XMLHTTP');
    } // 规范写法可以纯大小写或驼峰，但不能乱七八糟写


    var xhrParam = {}; // 最终xhr使用的参数

    var defaultOption = {
      url: '',
      async: true // 默认异步
      ,
      timeout: 0 // 默认不超时
      ,
      headers: {},
      data: {},
      dataType: ['*', 'text', 'Text', 'TEXT', 'json', 'Json', 'JSON', 'xml', 'Xml', 'XML'] // 获取的数据类型
      ,
      type: ['get', 'Get', 'GET', 'post', 'Post', 'POST', 'put', 'Put', 'PUT', 'delete', 'Delete', 'DELETE'] // 请求的类型
      ,
      beforeSend: function beforeSend() {},
      success: function success() {},
      error: function error() {},
      complete: function complete() {}
    };
    defaultOption = Object.entries(defaultOption);

    var _iterator9 = _createForOfIteratorHelper(defaultOption),
        _step9;

    try {
      for (_iterator9.s(); !(_step9 = _iterator9.n()).done;) {
        var _item = _step9.value;
        var _k = _item[0],
            _v = _item[1],
            ov = option.hasOwnProperty(_k) ? option[_k] : null;

        switch (_k) {
          case 'type':
          case 'dataType':
            // 数组取默认值类型
            xhrParam[_k] = (ov === null || !_v.includes(ov) ? _v[0] : ov).toUpperCase(); // 最终处理成大写

            break;

          case 'async':
            // 布尔类型
            xhrParam[_k] = ov === null ? _v : !!ov;
            break;

          case 'data':
          case 'headers':
            // xhrParam[k] = (ov === null || ov.constructor !== Object)?v:ov;
            xhrParam[_k] = ov === null ? _v : ov;
            break;

          default:
            xhrParam[_k] = ov === null ? _v : ov;
        }
      } // 判断是get还是post,如果是get则需要将其拼接到url上并置空

    } catch (err) {
      _iterator9.e(err);
    } finally {
      _iterator9.f();
    }

    var dataQuery = _typeof(xhrParam.data) == 'object' ? zslBase.httpBuildQuery(xhrParam.data) : xhrParam.data;

    if (xhrParam.type === 'GET') {
      // 移除右侧?和&
      xhrParam.url = zslBase.rtrim(zslBase.rtrim(xhrParam.url, '?'), '&'); // 移除右侧的'?'和'&'

      xhrParam.url += (xhrParam.url.includes('?') ? '&' : '?') + dataQuery; // 拼接到url

      xhrParam.data = ''; // 清空
    } else {
      if (xhrParam.dataType == 'JSON') {
        xhrParam.data = JSON.stringify(xhrParam.data);
        xhrParam.headers['Content-Type'] = "application/json; charset=UTF8";
      } else {
        xhrParam.data = dataQuery;
        xhrParam.headers['Content-Type'] = "application/x-www-form-urlencoded";
      } // xhrParam.headers['X-HTTP-Method-Override'] = xhrParam.type;
      // xhrParam.type = "POST";

    } // 超时时间是数字并且大于0则设置超时


    if (zslBase.isNumeric(xhrParam.timeout) && xhrParam.timeout > 0) {
      xhr.timeout = parseInt(xhrParam.timeout);

      xhr.ontimeout = function (e) {
        // https://developer.mozilla.org/zh-CN/docs/Web/API/XMLHttpRequest/abort
        xhr.abort();
      };
    } // 设置请求方式


    xhr.open(xhrParam.type.toUpperCase(), xhrParam.url, xhrParam.async); // 设置监听事件

    xhr.onreadystatechange = function () {
      // 状态发生变化时，函数被回调
      if (xhr.readyState === 4) {
        // 成功完成
        xhrParam.complete(); // 判断响应结果:

        if (xhr.status === 200) {
          // 成功，通过responseText拿到响应的文本:
          var result = xhr.responseText,
              dataType = xhrParam.dataType.toUpperCase(); // 转换成大写，规范处理

          if (dataType === '*') {
            // 自动类型，默认json判断，如果不是进行xml处理，如果xml也失败了则返回原始字符串
            result = zslBase.isJson(result) ? JSON.parse(result) : result;
          }

          if (dataType === 'JSON') {
            // JSON数据
            result = zslBase.isJson(result) ? JSON.parse(result) : result;
          } else if (dataType === 'XML') {
            // XML数据
            result = zslBase.parseXML(result);
          }

          xhrParam.success(result, xhr);
        } else {
          // 失败，根据响应码判断失败原因:
          setTimeout(function () {
            // 失败时将失败处理放到异步栈里，不然onreadystatechange事件会优先于其他事件，导致某些事件判断错误或无法判断
            xhrParam.error(xhr.status, xhr);
          }, 1);
        }
      } else {// HTTP请求还在继续...
      }
    }; // 执行请求前操作


    xhrParam.beforeSend(xhr); // 遍历请求头(请求头的设置并需在open之后才能设置)

    for (var _i2 = 0, _Object$entries2 = Object.entries(xhrParam.headers); _i2 < _Object$entries2.length; _i2++) {
      var item = _Object$entries2[_i2];
      var k = item[0],
          v = item[1];
      xhr.setRequestHeader(k, v);
    } // 发送请求


    xhr.send(xhrParam.data);
  }; // get


  zslBase.get = function (url, fn, dataType) {
    zslBase.ajax({
      url: url,
      dataType: dataType,
      type: 'get',
      success: fn
    });
  }; // post


  zslBase.post = function (url, data, fn, dataType) {
    zslBase.ajax({
      url: url,
      data: data,
      dataType: dataType,
      type: 'post',
      success: fn
    });
  };
  /**
   * 生成随机数
   * range             int            如果只有一个参数则range是1到range,两个参数则随机range-maxNum
   * maxNum            int            最大
  .*/


  zslBase.random = function (range, maxNum) {
    switch (arguments.length) {
      case 1:
        // 如果只要参数1，则参数1表示随机范围的最大
        return parseInt(Math.random() * range + 1, 10);
        break;

      case 2:
        // 如果有两个参数则求这两个的数值范围
        return parseInt(Math.random() * (maxNum - range + 1) + range, 10);
        break;

      default:
        return parseInt(Math.random() * 10); // 没传参数则返回0-9

        break;
    }
  };
  /**
   * 随机字符串
   * length             int            生成的字符串长度
   * pattern            int            字符串模式1:仅数字，10仅小写字母，100仅大写，……
  .*/


  zslBase.randomString = function (length, pattern) {
    var chars = '';
    var allChars = {
      number: '0123456789',
      letters: 'abcdefghijklmnopqrstuvwxyz'
    };
    pattern = pattern ? pattern.toString() : ''; // 按位数写法,个位代表数字，十位代表小写，百位代表大写……

    switch (pattern) {
      case '1':
        // 仅数字
        chars = allChars['number'];
        break;

      case '10':
        // 仅小写字母
        chars = allChars['letters'];
        break;

      case '100':
        // 仅大写字母
        chars = allChars['letters'].toUpperCase();
        break;

      case '11':
        // 数字+小写
        chars = allChars['number'] + allChars['letters'];
        break;

      case '101':
        // 数字+大写
        chars = allChars['number'] + allChars['letters'].toUpperCase();
        break;

      case '110':
        // 大小写
        chars = allChars['letters'] + allChars['letters'].toUpperCase();
        break;

      default:
        chars = pattern ? pattern : allChars['number'] + allChars['letters'] + allChars['letters'].toUpperCase();
        break;
    }

    var charsLength = chars.length;
    var str = '';

    for (var i = 0; i < length; i++) {
      str += chars.charAt(Math.floor(Math.random() * charsLength));
    }

    return str;
  }; // uint8array转为base64字符串


  zslBase.uint8arrayToBase64 = function (u8Arr) {
    var CHUNK_SIZE = 0x8000; //arbitrary number

    var index = 0;
    var length = u8Arr.length;
    var result = '';
    var slice;

    while (index < length) {
      slice = u8Arr.subarray(index, Math.min(index + CHUNK_SIZE, length));
      result += String.fromCharCode.apply(null, slice);
      index += CHUNK_SIZE;
    } // web image base64图片格式: "data:image/png;base64," + b64encoded;
    // return  "data:image/png;base64," + btoa(result);


    return btoa(result);
  }; // base64字符串转为uint8array数组


  zslBase.base64ToUint8Array = function (base64String) {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding).replace(/\-/g, '+').replace(/_/g, '/');
    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }

    return outputArray;
  }; // csv转数组


  var csvToArray = function csvToArray(strData, strDelimiter) {
    // Check to see if the delimiter is defined. If not,then default to comma.
    // 检查是否定义了分隔符。如果未定义，则默认为逗号
    strDelimiter = strDelimiter || ","; // Create a regular expression to parse the CSV values.
    // 创建一个正则表达式来解析CSV值。

    var objPattern = new RegExp( // Delimiters.(分隔符)
    "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" + // Quoted fields.(引用的字段)
    "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" + // Standard fields.(标准字段)
    "([^\"\\" + strDelimiter + "\\r\\n]*))", "gi"); // Create an array to hold our data. Give the array a default empty first row.
    // 创建一个数组来保存数据。给数组赋值一个空元素数组

    var arrData = [[]]; // Create an array to hold our individual pattern matching groups.
    // 创建一个数组来保存我们的单个模式匹配组

    var arrMatches = null; // Keep looping over the regular expression matches until we can no longer find a match.
    // 正则表达式匹配上保持循环,直到我们再也找不到匹配的

    while (arrMatches = objPattern.exec(strData)) {
      // Get the delimiter that was found.
      // 获取找到的分隔符
      var strMatchedDelimiter = arrMatches[1]; // Check to see if the given delimiter has a length
      // 检查给定的分隔符是否有长度
      // (is not the start of string) and if it matches
      // （不是字符串的开头）如果匹配
      // field delimiter. If id does not, then we know
      // 字段分隔符。如果身份证没有，那我们就知道了
      // that this delimiter is a row delimiter.
      // 此分隔符是行分隔符。

      if (strMatchedDelimiter.length && strMatchedDelimiter != strDelimiter) {
        // Since we have reached a new row of data,add an empty row to our data array.
        // 既然我们得到了新的一行数据，向数据数组中添加一个空行。
        arrData.push([]);
      } // Now that we have our delimiter out of the way,
      // 既然我们已经把分隔符弄出来了，
      // let's check to see which kind of value we
      // 让我们来看看我们需要什么样的价值观
      // captured (quoted or unquoted).
      // 捕获（引用或未引用）。


      var strMatchedValue = void 0;

      if (arrMatches[2]) {
        // We found a quoted value. When we capture this value, unescape any double quotes.
        // 我们找到一个引用值。当我们抓到这个值，取消显示任何双引号
        strMatchedValue = arrMatches[2].replace(new RegExp("\"\"", "g"), "\"");
      } else {
        // We found a non-quoted value.
        // 我们找到了一个没有引号的值。
        strMatchedValue = arrMatches[3];
      } // Now that we have our value string, let's add it to the data array.
      // 现在我们有了值字符串，让我们将其添加到数据数组中


      arrData[arrData.length - 1].push(strMatchedValue);
    } // 移除最后一个空数据


    arrData.splice(-1); // Return the parsed data.
    // 返回解析结果

    return arrData;
  }; // csv转对象


  zslBase.csvToObject = function (csv) {
    var array = csvToArray(csv);
    var objArray = [];

    for (var i = 1; i < array.length; i++) {
      objArray[i - 1] = {};

      for (var k = 0; k < array[0].length && k < array[i].length; k++) {
        var key = array[0][k];
        objArray[i - 1][key] = array[i][k];
      }
    }

    return objArray;
  }; // 深度冻结对象


  zslBase.deepFreeze = function (obj) {
    Object.freeze(obj);
    var keys = Object.keys(obj);

    for (var _i3 = 0, _keys = keys; _i3 < _keys.length; _i3++) {
      var k = _keys[_i3];

      if (obj[k] && !['_zsl_', '_ZSL_'].includes(k) && _typeof(obj[k]) === 'object') {
        zslBase.deepFreeze(obj[k]);
      }
    }
  }; // pako加密


  zslBase.pako_encode = function (rawData) {
    if ((typeof pako === "undefined" ? "undefined" : _typeof(pako)) == 'object' && typeof pako.deflateRaw == 'function') {
      if (zsl.empty(rawData)) {
        return '';
      }

      if (typeof rawData !== 'string') {
        rawData = JSON.stringify(rawData);
      }

      var binaryString = pako.deflateRaw(rawData, {
        to: 'string'
      });
      return zslBase.uint8arrayToBase64(binaryString);
    } else {
      throw '请先加载pako脚本';
    }
  }; // pako解密


  zslBase.pako_decode = function (str) {
    if ((typeof pako === "undefined" ? "undefined" : _typeof(pako)) == 'object' && typeof pako.deflateRaw == 'function') {
      var result = pako.inflateRaw(zslBase.base64ToUint8Array(str), {
        to: 'string'
      });

      if (zslBase.isJson(result)) {
        result = JSON.parse(result);
      }

      return result;
    } else {
      throw '请先加载pako脚本';
    }
  }; // // pako加解密
  // if (typeof pako == 'object') {
  //     if (typeof pako.deflateRaw == 'function') {
  //         zslBase.pako_encode = function(rawData){
  //             if(zsl.empty(rawData)){
  //                 return '';
  //             }
  //             if(typeof rawData !== 'string'){
  //                 rawData = JSON.stringify(rawData);
  //             }
  //             let binaryString = pako.deflateRaw(rawData, { to: 'string' });
  //             return zslBase.uint8arrayToBase64(binaryString);
  //         }
  //     }
  //     if (typeof pako.inflateRaw == 'function') {
  //         zslBase.pako_decode = function(str){
  //             return pako.inflateRaw(zslBase.base64ToUint8Array(str), {to: 'string'});
  //         }
  //     }
  // }
  // md5加密


  zslBase.md5 = function (e) {
    function t(e, t) {
      return e << t | e >>> 32 - t;
    }

    function n(e, t) {
      var n, r, o, i, a;
      return o = 2147483648 & e, i = 2147483648 & t, a = (1073741823 & e) + (1073741823 & t), (n = 1073741824 & e) & (r = 1073741824 & t) ? 2147483648 ^ a ^ o ^ i : n | r ? 1073741824 & a ? 3221225472 ^ a ^ o ^ i : 1073741824 ^ a ^ o ^ i : a ^ o ^ i;
    }

    function r(e, r, o, i, a, s, c) {
      return e = n(e, n(n(function (e, t, n) {
        return e & t | ~e & n;
      }(r, o, i), a), c)), n(t(e, s), r);
    }

    function o(e, r, o, i, a, s, c) {
      return e = n(e, n(n(function (e, t, n) {
        return e & n | t & ~n;
      }(r, o, i), a), c)), n(t(e, s), r);
    }

    function i(e, r, o, i, a, s, c) {
      return e = n(e, n(n(function (e, t, n) {
        return e ^ t ^ n;
      }(r, o, i), a), c)), n(t(e, s), r);
    }

    function a(e, r, o, i, a, s, c) {
      return e = n(e, n(n(function (e, t, n) {
        return t ^ (e | ~n);
      }(r, o, i), a), c)), n(t(e, s), r);
    }

    function s(e) {
      var t,
          n = "",
          r = "";

      for (t = 0; 3 >= t; t++) {
        n += (r = "0" + (e >>> 8 * t & 255).toString(16)).substr(r.length - 2, 2);
      }

      return n;
    }

    var c, u, l, f, d, p, h, v, m, g;

    for (g = function (e) {
      for (var t, n = e.length, r = n + 8, o = 16 * ((r - r % 64) / 64 + 1), i = new Array(o - 1), a = 0, s = 0; n > s;) {
        a = s % 4 * 8, i[t = (s - s % 4) / 4] = i[t] | e.charCodeAt(s) << a, s++;
      }

      return a = s % 4 * 8, i[t = (s - s % 4) / 4] = i[t] | 128 << a, i[o - 2] = n << 3, i[o - 1] = n >>> 29, i;
    }(e = function (e) {
      e = e.replace(/\r\n/g, "\n");

      for (var t = "", n = 0; n < e.length; n++) {
        var r = e.charCodeAt(n);
        128 > r ? t += String.fromCharCode(r) : r > 127 && 2048 > r ? (t += String.fromCharCode(r >> 6 | 192), t += String.fromCharCode(63 & r | 128)) : (t += String.fromCharCode(r >> 12 | 224), t += String.fromCharCode(r >> 6 & 63 | 128), t += String.fromCharCode(63 & r | 128));
      }

      return t;
    }(e)), p = 1732584193, h = 4023233417, v = 2562383102, m = 271733878, c = 0; c < g.length; c += 16) {
      u = p, l = h, f = v, d = m, p = r(p, h, v, m, g[c + 0], 7, 3614090360), m = r(m, p, h, v, g[c + 1], 12, 3905402710), v = r(v, m, p, h, g[c + 2], 17, 606105819), h = r(h, v, m, p, g[c + 3], 22, 3250441966), p = r(p, h, v, m, g[c + 4], 7, 4118548399), m = r(m, p, h, v, g[c + 5], 12, 1200080426), v = r(v, m, p, h, g[c + 6], 17, 2821735955), h = r(h, v, m, p, g[c + 7], 22, 4249261313), p = r(p, h, v, m, g[c + 8], 7, 1770035416), m = r(m, p, h, v, g[c + 9], 12, 2336552879), v = r(v, m, p, h, g[c + 10], 17, 4294925233), h = r(h, v, m, p, g[c + 11], 22, 2304563134), p = r(p, h, v, m, g[c + 12], 7, 1804603682), m = r(m, p, h, v, g[c + 13], 12, 4254626195), v = r(v, m, p, h, g[c + 14], 17, 2792965006), p = o(p, h = r(h, v, m, p, g[c + 15], 22, 1236535329), v, m, g[c + 1], 5, 4129170786), m = o(m, p, h, v, g[c + 6], 9, 3225465664), v = o(v, m, p, h, g[c + 11], 14, 643717713), h = o(h, v, m, p, g[c + 0], 20, 3921069994), p = o(p, h, v, m, g[c + 5], 5, 3593408605), m = o(m, p, h, v, g[c + 10], 9, 38016083), v = o(v, m, p, h, g[c + 15], 14, 3634488961), h = o(h, v, m, p, g[c + 4], 20, 3889429448), p = o(p, h, v, m, g[c + 9], 5, 568446438), m = o(m, p, h, v, g[c + 14], 9, 3275163606), v = o(v, m, p, h, g[c + 3], 14, 4107603335), h = o(h, v, m, p, g[c + 8], 20, 1163531501), p = o(p, h, v, m, g[c + 13], 5, 2850285829), m = o(m, p, h, v, g[c + 2], 9, 4243563512), v = o(v, m, p, h, g[c + 7], 14, 1735328473), p = i(p, h = o(h, v, m, p, g[c + 12], 20, 2368359562), v, m, g[c + 5], 4, 4294588738), m = i(m, p, h, v, g[c + 8], 11, 2272392833), v = i(v, m, p, h, g[c + 11], 16, 1839030562), h = i(h, v, m, p, g[c + 14], 23, 4259657740), p = i(p, h, v, m, g[c + 1], 4, 2763975236), m = i(m, p, h, v, g[c + 4], 11, 1272893353), v = i(v, m, p, h, g[c + 7], 16, 4139469664), h = i(h, v, m, p, g[c + 10], 23, 3200236656), p = i(p, h, v, m, g[c + 13], 4, 681279174), m = i(m, p, h, v, g[c + 0], 11, 3936430074), v = i(v, m, p, h, g[c + 3], 16, 3572445317), h = i(h, v, m, p, g[c + 6], 23, 76029189), p = i(p, h, v, m, g[c + 9], 4, 3654602809), m = i(m, p, h, v, g[c + 12], 11, 3873151461), v = i(v, m, p, h, g[c + 15], 16, 530742520), p = a(p, h = i(h, v, m, p, g[c + 2], 23, 3299628645), v, m, g[c + 0], 6, 4096336452), m = a(m, p, h, v, g[c + 7], 10, 1126891415), v = a(v, m, p, h, g[c + 14], 15, 2878612391), h = a(h, v, m, p, g[c + 5], 21, 4237533241), p = a(p, h, v, m, g[c + 12], 6, 1700485571), m = a(m, p, h, v, g[c + 3], 10, 2399980690), v = a(v, m, p, h, g[c + 10], 15, 4293915773), h = a(h, v, m, p, g[c + 1], 21, 2240044497), p = a(p, h, v, m, g[c + 8], 6, 1873313359), m = a(m, p, h, v, g[c + 15], 10, 4264355552), v = a(v, m, p, h, g[c + 6], 15, 2734768916), h = a(h, v, m, p, g[c + 13], 21, 1309151649), p = a(p, h, v, m, g[c + 4], 6, 4149444226), m = a(m, p, h, v, g[c + 11], 10, 3174756917), v = a(v, m, p, h, g[c + 2], 15, 718787259), h = a(h, v, m, p, g[c + 9], 21, 3951481745), p = n(p, u), h = n(h, l), v = n(v, f), m = n(m, d);
    }

    return (s(p) + s(h) + s(v) + s(m)).toLowerCase();
  }; // base64加密


  zslBase.base64_encode = function (rawStr) {
    if (typeof rawStr === 'undefined') {
      return null;
    }

    return window.btoa(unescape(encodeURIComponent(JSON.stringify(rawStr))));
  }; // base64解密


  zslBase.base64_decode = function (base64Str) {
    if (base64Str) {
      // 解密可能存在错误，所以需要try
      try {
        var parsedStr = decodeURIComponent(escape(window.atob(base64Str)));

        if (zslBase.isJson(parsedStr)) {
          // 有的字符串不可以json恢复
          parsedStr = JSON.parse(parsedStr);
        }

        return parsedStr;
      } catch (e) {}
    }

    return null;
  }; // // base64加密、解密
  // if (typeof CryptoJS == 'object') {
  //     if (typeof CryptoJS.enc.Base64 == 'object') {
  //         // base64加解密
  //         zslBase.base64_encode = function(rawStr, salt = ''){
  //             if(!rawStr){
  //                 return null;
  //             }
  //             if(typeof rawStr !== 'string'){
  //                 rawStr = JSON.stringify(rawStr);
  //             }
  //             let wordArray = CryptoJS.enc.Utf8.parse(rawStr);
  //             let base64 = CryptoJS.enc.Base64.stringify(wordArray);
  //             return base64;
  //         }
  //         // base64解密
  //         zslBase.base64_decode = function(base64, salt = ''){
  //             if(base64){
  //                 try {
  //                     let parsedWordArray = CryptoJS.enc.Base64.parse(base64);
  //                     let parsedStr = parsedWordArray.toString(CryptoJS.enc.Utf8);
  //                     if(zsl.isJson(parsedStr)){
  //                         parsedStr = JSON.parse(parsedStr);
  //                     }
  //                     return parsedStr;
  //                 } catch(e) {}
  //             }
  //             return null;
  //         }
  //     }
  // }
  // 版本号比较，x > y =>1, x < y => -1, x = y => 0


  zslBase.versionContrast = function (x, y) {
    x = x.toString().split('.');
    y = y.toString().split('.');
    var x_len = x.length,
        y_len = y.length,
        len = x_len > y_len ? x_len : y_len;

    for (var i = 0; i < len; i++) {
      if (i >= y_len || x[i] > y[i]) {
        return 1;
      } else if (i >= x_len || x[i] < y[i]) {
        return -1;
      }
    }

    return 0;
  }; // 浏览器画布指纹


  zslBase.canvasFingerprint = function () {
    function bin2hex(str) {
      var result = "";

      for (var i = 0; i < str.length; i++) {
        result += int16_to_hex(str.charCodeAt(i));
      }

      return result;
    }

    function int16_to_hex(i) {
      var result = i.toString(16);
      var j = 0;

      while (j + result.length < 4) {
        result = "0" + result;
        j++;
      }

      return result;
    }

    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');
    var txt = zslBase.author; // 'slongzhang@qq.com'

    ctx.textBaseline = "top";
    ctx.font = "14px 'Arial'";
    ctx.textBaseline = zslBase.author.split('@')[0];
    ctx.fillStyle = "#f60";
    ctx.fillRect(125, 1, 62, 20);
    ctx.fillStyle = "#069";
    ctx.fillText(txt, 2, 15);
    ctx.fillStyle = "rgba(102, 204, 0, 0.7)";
    ctx.fillText(txt, 4, 17);
    var b64 = canvas.toDataURL().replace("data:image/png;base64,", "");
    var bin = atob(b64);
    var crc = bin2hex(bin.slice(-16, -12));
    return crc;
  }; // 常见表单正则( 用户名，手机号，密码强度，身份证号，固定电话/座机 )


  zslBase.check = {
    number: function number(val) {
      // 是否纯数字(可整型也可浮点型)
      try {
        return val !== "" && val !== null && !isNaN(val);
      } catch (error) {}

      return false;
    },
    integer: function integer(val) {
      // 是否整型
      try {
        return val % 1 === 0;
      } catch (error) {}

      return false;
    },
    "float": function float(val) {
      // 是否浮点型
      try {
        return val !== "" && val !== null && !isNaN(val) && val % 1 !== 0;
      } catch (error) {}

      return false;
    }
    /**
     * 邮箱验证（仅支持常规验证，不支持中文域名、中文邮箱）
     */
    ,
    email: function email(_email) {
      // 常规正则+过滤特殊情况(顶级域名后缀只能是纯字母)
      if (/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/.test(_email) && /\.[a-zA-Z]+$/.test(_email)) {
        return true;
      }

      return false;
    },
    username: function username() {// 用户名验证
    },
    mobile: function mobile(_phone) {
      // 手机号验证
      return /^1[3456789]\d{9}$/.test(_phone);
    }
    /**
     * 密码验证
     * pwd              String            要验证的密码
     * level            Int               验证级别，默认2，不能单一类型密码
     * min              Int               不能少于X位，默认6
     * max              Int               不能多于X位，默认16
     * chars            String            要使用的特殊符号，常见需要转义的特殊字符默认已添加处理了，如有遗落自行处理handleChar函数；
     */
    ,
    password: function password(pwd) {
      var level = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
      var min = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 6;
      var max = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 16;
      var chars = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : ',.!@#$%^&*';
      var result = false,
          reg,
          psChars = '';

      try {
        // 防止出现乱七八糟的参数
        // 转义特殊字符
        var handleChar = function handleChar(_str) {
          _str = _str.split('');
          var zy = regEscapeCharacter,
              ps = '';

          var _iterator10 = _createForOfIteratorHelper(_str),
              _step10;

          try {
            for (_iterator10.s(); !(_step10 = _iterator10.n()).done;) {
              var v = _step10.value;

              if (zy.includes(v)) {
                ps += '\\\\';
              }

              ps += v;
            }
          } catch (err) {
            _iterator10.e(err);
          } finally {
            _iterator10.f();
          }

          return ps;
        };

        switch (level) {
          case 0:
            // 字母+数字,位数在min到max之间即可(可单一类型)
            reg = new RegExp('^[a-zA-Z0-9]{' + min + ',' + max + '}$');
            result = reg.test(pwd);
            break;

          case 1:
            // 字母+数字+特殊字符,.!@#$%^&* ,位数在min到max之间即可(可单一类型)
            reg = new RegExp('^[a-zA-Z0-9,' + handleChar(chars) + ']{' + min + ',' + max + '}$');
            result = reg.test(pwd);
            break;

          case 2:
            // 字母+数字+特殊字符,.!@#$%^&*，必须两种及以上组合(可字母数字组合，字母特殊字符组合，数字和特殊字符组合，大写和小写组合)
            psChars = handleChar(chars);
            reg = new RegExp('(?!^\\d+$)(?!^[a-z]+$)(?!^[A-Z]+$)(?!^[' + psChars + ']+$)(^[a-zA-Z0-9' + psChars + ']{' + min + ',' + max + '}$)');
            result = reg.test(pwd);
            break;

          case 3:
            // 字母+数字+特殊字符,.!@#$%^&*，必须两种及以上组合(可字母数字组合，字母特殊字符组合，数字和特殊字符组合)
            psChars = handleChar(chars);
            reg = new RegExp('(?!^\\d+$)(?!^[a-zA-Z]+$)(?!^[' + psChars + ']+$)(^[a-zA-Z0-9' + psChars + ']{' + min + ',' + max + '}$)');
            result = reg.test(pwd);
            break;

          default:
            // 任意字符只要符合位数即可
            reg = new RegExp('.{' + min + ',' + max + '}$');
            result = reg.test(pwd);
        }

        return result;
      } catch (error) {
        return false;
      }
    }
    /**
     * 身份证号验证
     *   结构和形式
     *     1．号码的结构 
     *       公民身份号码是特征组合码，由十七位数字本体码和一位校验码组成。排列顺序从左至右依次为：六位数字地址码，八位数字出生日期码，三位数字顺序码和一位数字校验码。 
     *     2．地址码 
     *       表示编码对象常住户口所在县（市、旗、区）的行政区划代码，按GB/T2260的规定执行。 
     *     3．出生日期码 
     *       表示编码对象出生的年、月、日，按GB/T7408的规定执行，年、月、日代码之间不用分隔符。 
     *     4．顺序码 
     *       表示在同一地址码所标识的区域范围内，对同年、同月、同日出生的人编定的顺序号，顺序码的奇数分配给男性，偶数分配给女性。 
     *     5．校验码
     *       根据前面十七位数字码，按照ISO 7064:1983.MOD 11-2校验码计算出来的检验码。
     *
     *  计算方法
     *    1、将前面的身份证号码17位数分别乘以不同的系数。从第一位到第十七位的系数分别为：7－9－10－5－8－4－2－1－6－3－7－9－10－5－8－4－2。
     *    2、将这17位数字和系数相乘的结果相加。
     *    3、用加出来和除以11，看余数是多少？
     *    4、余数只可能有0－1－2－3－4－5－6－7－8－9－10这11个数字。其分别对应的最后一位身份证的号码为1－0－X －9－8－7－6－5－4－3－2。
     *    5、通过上面得知如果余数是3，就会在身份证的第18位数字上出现的是9。如果对应的数字是2，身份证的最后一位号码就是罗马数字x。
     *    例如：某男性的身份证号码为【53010219200508011x】， 我们看看这个身份证是不是合法的身份证。
     *    首先我们得出前17位的乘积和【(5*7)+(3*9)+(0*10)+(1*5)+(0*8)+(2*4)+(1*2)+(9*1)+(2*6)+(0*3)+(0*7)+(5*9)+(0*10)+(8*5)+(0*8)+(1*4)+(1*2)】是189，然后用189除以11得出的结果是189/11=17----2，也就是说其余数是2。最后通过对应规则就可以知道余数2对应的检验码是X。所以，可以判定这是一个正确的身份证号码。
     */
    ,
    IDnumber: function IDnumber(id) {
      //身份证正则表达式(18位) 
      var reg = /^[1-9]\d{5}(19\d{2}|[2-9]\d{3})((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])(\d{4}|\d{3}X)$/i;
      var stard = "10X98765432"; //最后一位身份证的号码

      var first = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]; //1-17系数

      var sum = 0;

      if (!reg.test(id)) {
        return false;
      }

      var year = id.substr(6, 4);
      var month = id.substr(10, 2);
      var day = id.substr(12, 2);
      var birthday = id.substr(6, 8); //校验日期是否合法

      if (birthday != zslBase.date('Ymd', new Date(year + '/' + month + '/' + day))) {
        return false;
      }

      for (var i = 0; i < id.length - 1; i++) {
        sum += id[i] * first[i];
      }

      var result = sum % 11;
      var last = stard[result]; //计算出来的最后一位身份证号码

      if (id[id.length - 1].toUpperCase() == last) {
        return true;
      } else {
        return false;
      }
    }
    /**
     * 匹配国内电话号码(0511-4405222 或 021-87888822) 
     */
    ,
    landline: function landline(v) {
      return /\d{3}-\d{8}|\d{4}-\d{7}/.test(v);
    }
  }; // 用户自定义更多的扩展

  zslBase.ed = {};
  zslBase.empty = empty;
  var // Map over zslBase in case of overwrite
  _zslBase = window.zslBase,
      // Map over the zsl in case of overwrite
  _zsl = window.zsl; // 解除zsl和zslBase变量的使用

  zslBase.noConflict = function (deep) {
    if (window.zsl === zslBase) {
      window.zsl = _zsl;
    }

    if (deep && window.zslBase === zslBase) {
      window.zslBase = _zslBase;
    }

    return zslBase;
  };

  if (typeof noGlobal === "undefined") {
    window.zslBase = window.zsl = zslBase;
  } // 开始冻结对象，除了ed可以自己扩展外其他的不允许再添加或修改了


  Object.freeze(zslBase);
  Object.freeze(zslBase.check);
  return zslBase;
});
